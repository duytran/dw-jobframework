/* global require module */
'use strict';

var Mail = require('dw/net/Mail');
var Logger = require('dw/system/Logger');

function execute( args )
{
	Logger.getLogger("CustomStepEmail").debug(args);
	var mail = new Mail();
	mail.addTo("duy.tran@bluecomgroup.com");
	mail.setFrom("demo.dustin@bluecomgroup.com");
	mail.setSubject("Example Email");
	// sets the content of the mail as plain string
	var body = "This is a demo email with body: ";
	if (args.Content) { //"Content" is a parameter name for the step (system and custom).
		body += args.Content; //or using args["Content"] for parameter name with space
	}
	else {
		body += "content is not available";
	}
		
	mail.setContent(body); 
	return mail.send();
	//return null;
}

module.exports = {
	execute: execute
};